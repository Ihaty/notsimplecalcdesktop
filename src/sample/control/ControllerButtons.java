package sample.control;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

//Вынести методы отдельно от инстансов!
//Приватные поля/методы!
//Повторяющиеся inputAndOutputField!
//return излишний код if-else!
//Проверки на null
// = если не сплит (0 пофиксил, теперь остальные числа)!

public class ControllerButtons {
    String fieldToString;

    @FXML
    private TextField inputAndOutputField;

    @FXML
    private Button root;

    @FXML
    private Button off;

    @FXML
    private Button sin;

    @FXML
    private Button ln;

    @FXML
    private Button nine;

    @FXML
    private Button eight;

    @FXML
    private Button six;

    @FXML
    private Button three;

    @FXML
    private Button zeroTwo;

    @FXML
    private Button five;

    @FXML
    private Button two;

    @FXML
    private Button zero;

    @FXML
    private Button seven;

    @FXML
    private Button four;

    @FXML
    private Button one;

    @FXML
    private Button point;

    @FXML
    private Button asin;

    @FXML
    private Button cos;

    @FXML
    private Button tg;

    @FXML
    private Button acos;

    @FXML
    private Button atg;

    @FXML
    private Button ctg;

    @FXML
    private Button log;

    @FXML
    private Button actg;

    @FXML
    private Button split;

    @FXML
    private Button percent;

    @FXML
    private Button multiply;

    @FXML
    private Button minus;

    @FXML
    public Button plus;

    @FXML
    private Button equals;

    @FXML
    private Button plusMinus;

    @FXML
    private Button clearAll;

    @FXML
    private Button clear;

    public void initialize(){
        inputAndOutputField.setText("0");
    }

    //Сократить в один метод!
    @FXML
    void inputFieldZero() {
        insertNumbers("0");
    }

    @FXML
    void inputFieldZeroTwo() {
        insertNumbers("00");
    }

    @FXML
    void inputFieldOne() {
        insertNumbers("1");
    }

    @FXML
    void inputFieldTwo() {
        insertNumbers("2");
    }

    @FXML
    void inputFieldThree() {
        insertNumbers("3");
    }

    @FXML
    void inputFieldFour() {
        insertNumbers("4");
    }

    @FXML
    void inputFieldFive() {
        insertNumbers("5");
    }

    public void inputFieldSix() {
        insertNumbers("6");
    }

    @FXML
    void inputFieldSeven() {
        insertNumbers("7");
    }

    @FXML
    void inputFieldEight() {
        insertNumbers("8");
    }

    @FXML
    void inputFieldNine() {
        insertNumbers("9");
    }

    @FXML
    void result() {
        if (inputAndOutputField.getText().equals("0")) {
            return;
        }
        int convertIntNum = 0;
        String[] splitNumber = inputAndOutputField.getText().split("\\+");
        for (int i = 0; i < splitNumber.length; i++) {
            int preConvert = Integer.parseInt(splitNumber[i]);
            convertIntNum += preConvert;
        }
        inputAndOutputField.setText(String.valueOf(convertIntNum));
    }

    @FXML
    void plusNumber() {
        insertOperators("+");
    }

    @FXML
    void minusNumber() {
        if (inputAndOutputField.getText().equals("0")) {
            inputAndOutputField.setText("-");
            return;
        }
        inputAndOutputField.setText(inputAndOutputField.getText() + "-");
    }

    @FXML
    void multiplyNumber() {
        System.out.println("dadad");

    }

    @FXML
    void percentNumber() {
        System.out.println("dadad");

    }

    @FXML
    void splitNumber() {
        System.out.println("dadad");

    }

    @FXML
    void clear() {
        fieldToString = inputAndOutputField.getText();
            inputAndOutputField.setText(fieldToString.substring(0, fieldToString.length() - 1));
            if (fieldToString.length() == 1) {
                inputAndOutputField.setText("0");
            }
    }

    @FXML
    void clearAll() {
        inputAndOutputField.setText("0");
    }

    void insertOperators(String operator) {
        fieldToString = inputAndOutputField.getText();
        if (fieldToString.equals("0")) {
            inputAndOutputField.setText(operator);
            return;
        }
        inputAndOutputField.setText(fieldToString + operator);
        Matcher m = Pattern.compile("(\\+)\\1+").matcher(fieldToString);
        if (m.find()) {
            inputAndOutputField.setText(fieldToString.substring(0, fieldToString.length() - 1));
        }
    }

    void insertNumbers(String num) {
        if (inputAndOutputField.getText().equals("0")) {
            inputAndOutputField.setText(num);
        } else {
            inputAndOutputField.setText(inputAndOutputField.getText() + num);
        }
    }

    //insertSymbols
}
