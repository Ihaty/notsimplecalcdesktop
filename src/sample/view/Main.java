package sample.view;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import sample.control.ControllerButtons;

public class Main extends Application {
    ControllerButtons controllerButtons = new ControllerButtons();
    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Engineering calculator");
        primaryStage.setScene(new Scene(root, 512, 350));
        primaryStage.show();
        primaryStage.setResizable(false);
        getClass().getResource(
                "sample.fxml"
        );
    }

    public static void main(String[] args) {
        launch(args);
    }
}
